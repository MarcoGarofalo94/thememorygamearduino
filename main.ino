#include "WiFiEsp.h"
#include <ArduinoJson.h>
#include <LedControl.h>
#include "pitches.h"

#ifndef HAVE_HWSERIAL1
#include "SoftwareSerial.h"
// set up software serial to allow serial communication to our TX and RX pins
SoftwareSerial Serial1(10, 11);
#endif

#define BUZZER 2
// Set  baud rate of so we can monitor output from esp.
#define ESP8266_BAUD 9600
//Matrix initialization pins
#define DIN 9
#define CS 7
#define CLK 8
#define ROWS 8

// Global Declarative Part
unsigned long int previousMillis = 0,currentMillis;
unsigned int sprite = 0;
unsigned int isExpired = 0;
LedControl lc=LedControl(DIN,CLK,CS,1);
byte figure[] = {B00000000, B00000000, B00000000, B00000000, B00000000, B00000000, B00000000, B00000000 };
WiFiEspServer server(80); // Define an esp server that will listen on port 80

void setup()
{
    char ssid[] = "FASTWEB-DAFD5F2";
    char pass[] = "1Z1NHH38PK";
    int status = WL_IDLE_STATUS;
    // Open up communications for arduino serial and esp serial at same rate
    Serial.begin(ESP8266_BAUD);
    Serial1.begin(ESP8266_BAUD);
    
    // Initialize the esp module
    WiFi.init(&Serial1);

    // Start connecting to wifi network and wait for connection to complete
    while (status != WL_CONNECTED)
    {
        Serial.print(F("Conecting to wifi network: "));
        Serial.println(ssid);
        status = WiFi.begin(ssid, pass);
    }

    // Once we are connected log the IP address of the ESP module
    Serial.print(F("IP Address of ESP8266 Module is: "));
    Serial.println(WiFi.localIP());
    Serial.println(F("You're connected to the network"));
    
    // Start the server
    server.begin();
    init(server, WiFi.localIP()); // Sending the ip address to the proxy.
}

//  Continually check for new clients
void loop()
{ 
   WiFiEspClient client = server.available();
   currentMillis = millis();

   checkExpiredTime(); //check if 35seconds have been passed before receiving an answer from the client
   checkStartTime(); //check if 5seconds have been passed 

    // If a client has connected...
    if (client)
    {
        String json = F("");
        Serial.println(F("A client has connected"));

        while (client.connected())
        {
            // Read in json from connected client
            if (client.available())
            {
                // ignore headers and read to first json bracket
                client.readStringUntil('{');

                // get json body (everything inside of the main brackets)
                String jsonStrWithoutBrackets = client.readStringUntil('}');

                // Append brackets to make the string parseable as json
                String jsonStr = "{" + jsonStrWithoutBrackets + "}";
                // if we managed to properly form jsonStr...
                if (jsonStr.indexOf('{', 0) >= 0)
                {
                    // parse string into json, bufferSize calculated by https://arduinojson.org/v5/assistant/
                    const size_t bufferSize = JSON_OBJECT_SIZE(8) + JSON_OBJECT_SIZE(1) + 77;
                    DynamicJsonBuffer jsonBuffer(bufferSize);
                    const char *value,*pEnd;
                    JsonObject &root = jsonBuffer.parseObject(jsonStr);
                    //Serial.println(jsonStr);

                   //here is the check between the real matrix (figure) and the virtual one got form the json (rows)
                    int k = 0;
                    byte b;
                    boolean isCorrect = false;
                    for(k = 0; k < ROWS; k++){
                      value = root["rows"][k];
                      /*Serial.print(value );
                      Serial.print(", ");
                      Serial.println((byte)figure[k]);*/
                      b = (byte)strtol(value,&pEnd,2); //conversion of a binary string to byte
                    if(b == figure[k]){
                        isCorrect = true;
                      }else{
                        isCorrect = false; //if only one is false then there is no need to continue checking
                        break;
                      }
                    }

                    
                    if(isCorrect == true){ //giving a point
                       // send response and close connection
                       client.print(
                        F("HTTP/1.1 200 OK\r\n"
                        "Connection: close\r\n" // the connection will be closed after completion of the response
                        "Content-Type: application/json\r\n"
                        "Content-Length: 19\r\n"
                        "\r\n"
                        "{\"correct\":true}\r\n"
                        "\r\n")
                        );
                        drawV();
                        tone(BUZZER,NOTE_B6,200); //winning theme
                        delay(100);
                        tone(BUZZER,NOTE_E7,500);
                        delay(1000);
                        resetMatrix();
                        
                    }else{
                                             // send response and close connection
                    
                       client.print(
                        F("HTTP/1.1 200 OK\r\n"
                        "Connection: close\r\n" // the connection will be closed after completion of the response
                        "Content-Type: application/json\r\n"
                        "Content-Length: 20\r\n"
                        "\r\n"
                        "{\"correct\":false}\r\n"
                        "\r\n")
                        );
                        drawX();
                        losingTheme(); //play a losing theme
                        resetMatrix();
                        sprite = 0; //reset the difficult
                    }
         
                    client.stop(); //stop the client and be ready for another request
                    nextFigure(); // showing a new sprite on the matrix
                }
                else
                {
                    // we were unable to parse json, send http error status and close connection
                    client.print(
                        F("HTTP/1.1 500 ERROR\r\n"
                        "Connection: close\r\n"
                        "\r\n"));

                    Serial.println(F("Error, bad or missing json"));
                    client.stop();
                }
            }
        }
        Serial.println(F("Client disconnected"));
    }
}

void nextFigure(){
  unsigned int i = 0;

  for( unsigned int j  = 0; j < ROWS; j++){
    figure[j] = (byte) 0; //reset the figure array for future matches
  }
  
  for (i = 0; i< ROWS; i++){ // creating random sprite on the matrix, increasing gradually the difficulty.
    
    if(i % 2 == 0){
      figure[i] = (byte) random(255);
      lc.setRow(0,i,figure[i]);
    }else{
       figure[i] = (byte) 0;
    }
      
    if(i > sprite){ //resetting the content of figure after the difficulty considered and the matrix
        figure[i] = (byte) 0;
        lc.setRow(0,i,B00000000);
      }
  }
  
  sprite++; //increase difficulty
  sprite = (int) sprite % 8; //staying always between 8 rows
  
  currentMillis = millis(); //restarting the millis timer
  previousMillis = currentMillis; // and since when we should start compare
  isExpired = 0; //resetting isExpired variable
}

void resetMatrix(){
   lc.clearDisplay(0);
}


/**
  This function take as arguments the server and the ip address of the ESP8266
  and its purpose is to send an HTTP request to an online proxy in order to communicate 
  the ip address of the ESP8266.
*/
void init(WiFiEspServer server,IPAddress ip){

    //initialization of the matrix
    lc.shutdown(0,false);
    lc.setIntensity(0,1);
    lc.clearDisplay(0);

    String s=""; //converting the IPAddress to a string.
    for (int i=0; i<4; i++) {
      s += i  ? "." + String(ip[i]) : String(ip[i]);
    }

    WiFiEspClient client = server.available();
    if (client.connect("90.147.166.236", 8082)) {
      Serial.println(F("connected to server"));
  
      // Make a HTTP request:
      client.println(F("GET /init HTTP/1.1"));
      client.println(F("Host 90.147.166.236:8082"));
      client.println("bearer: "+s);
      client.println(F("Connection: close"));
      client.println();
  }
  
  client.stop();
  delay(10);
  randomSeed(analogRead(0));
  nextFigure(); //after closing the communication with the server, we start the game.
}

void losingTheme(){
  float duration = (750/4);
  float delayPause = duration * 1.30;
  unsigned int losingMelody[] = { NOTE_C5, NOTE_AS4, NOTE_A4,
                                  NOTE_A4, NOTE_G4, 0, 
                                  NOTE_G4, NOTE_D5, NOTE_C5, 
                                  0, NOTE_AS4, 0, NOTE_A4 };
                                  
  for(unsigned int i = 0; i < 13; i++){
    tone(BUZZER,losingMelody[i],duration);
    delay(delayPause);
  }
}

/**
  This function checks if have been passed 35 seconds since the matrix showed a sprite to reproduce in order to 
  stop the current match and start another one resetting the difficulty.
*/
void checkExpiredTime(){ 
  if(currentMillis - previousMillis >= 35000){
    drawX();
    losingTheme();
    resetMatrix();
    nextFigure();
    sprite = 0;
  }
}

/**
  This function checks if have been passed 5 seconds since the matrix showed a sprite to reproduce in order to clear
  the matrix and give the player 30 seconds to play. isExpired variable ensure us that this check will be done only the first time
*/
void checkStartTime(){
  if(currentMillis - previousMillis >= 5000 && isExpired == 0){
    
    tone(BUZZER,NOTE_CS7,500);
    delay(100);
    noTone(BUZZER);
    resetMatrix();
    isExpired = 1;
  }
}

void drawX(){
    byte fail[8] = {
      B00000000,
      B01000010,
      B00100100,
      B00011000,
      B00011000,
      B00100100,
      B01000010,
      B00000000
    };

    for(unsigned int i = 0; i < ROWS; i++){
      lc.setRow(0,i,fail[i]);
    }
}

void drawV(){
  byte right[8] = { 
     B00000000,
     B00000010,
     B01000110,
     B01101100,
     B00111000,
     B00010000,
     B00000000,
     B00000000
   };
   
   for(unsigned int i = 0; i < ROWS; i++){
    lc.setRow(0,i,right[i]);
   }
     
}
